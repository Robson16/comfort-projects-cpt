# Comfort Projects CPT

A WordPress Plugin to add a Custom Post Type named Projects

## Getting started

Add this plugin on your WordPress Installation on: 

```
\wp-content\plugins\comfort-projects-cpt

```

Then you will be able to use the new post type Projects after adding some of your projects, use the shortcode:

```
[comfort_projects]
```

This will display some HTML for them, you will need to provide the CSS to style the list.

There is some parameters that you can pass: 

```
[comfort_projects number_of_items="-1"]
```

- 'number_of_items' - The quantity of itens to show, pass -1 to retrieve all;
- 'orderby' - Use for order by some attribute of the post, like: date, title...;
- 'order' - Designates the ascending or descending order of the 'orderby' parameter. Defaults: 'ASC'.