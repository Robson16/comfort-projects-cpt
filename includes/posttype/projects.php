<?php

// Register Projects Custom Post Type
function comfort_project_post_type()
{
  $labels = array(
    'name'                  => _x('Projects', 'Post Type General Name', 'comfortprojects'),
    'singular_name'         => _x('Project', 'Post Type Singular Name', 'comfortprojects'),
    'menu_name'             => __('Projects', 'comfortprojects'),
    'name_admin_bar'        => __('Project', 'comfortprojects'),
    'archives'              => __('Project Archives', 'comfortprojects'),
    'attributes'            => __('Projects Attributes', 'comfortprojects'),
    'parent_item_colon'     => __('Parent Projects:', 'comfortprojects'),
    'all_items'             => __('All Projects', 'comfortprojects'),
    'add_new_item'          => __('Add New Project', 'comfortprojects'),
    'add_new'               => __('Add Project', 'comfortprojects'),
    'new_item'              => __('New Project', 'comfortprojects'),
    'edit_item'             => __('Edit Project', 'comfortprojects'),
    'update_item'           => __('Update Project', 'comfortprojects'),
    'view_item'             => __('View Project', 'comfortprojects'),
    'view_items'            => __('View Projects', 'comfortprojects'),
    'search_items'          => __('Search Project', 'comfortprojects'),
    'not_found'             => __('Not found', 'comfortprojects'),
    'not_found_in_trash'    => __('Not found in Trash', 'comfortprojects'),
    'featured_image'        => __('Featured Image', 'comfortprojects'),
    'set_featured_image'    => __('Set featured image', 'comfortprojects'),
    'remove_featured_image' => __('Remove featured image', 'comfortprojects'),
    'use_featured_image'    => __('Use as featured image', 'comfortprojects'),
    'insert_into_item'      => __('Insert into projects', 'comfortprojects'),
    'uploaded_to_this_item' => __('Uploaded to this project', 'comfortprojects'),
    'items_list'            => __('Projects list', 'comfortprojects'),
    'items_list_navigation' => __('Projects list navigation', 'comfortprojects'),
    'filter_items_list'     => __('Filter projects list', 'comfortprojects'),
  );

  $args = array(
    'label'                 => __('Project', 'comfortprojects'),
    'description'           => __('Comfort Projects', 'comfortprojects'),
    'labels'                => $labels,
    'supports'              => array('title', 'editor', 'excerpt', 'thumbnail', 'author', 'revisions'),
    'taxonomies'            => array('project_category'),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-portfolio',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
    'show_in_rest'          => true,
  );

  register_post_type('project', $args);
}

add_action('init', 'comfort_project_post_type', 0);
