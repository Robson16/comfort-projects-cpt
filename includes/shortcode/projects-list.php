<?php

// Add Projects List Shortcode
function comfort_projects_shortcode($atts)
{

  // Attributes
  $atts = shortcode_atts(
    array(
      'number_of_items'   => '4',
      'orderby'           => 'date',
      'order'             => 'ASC',

    ),
    $atts
  );

  // Custom query
  $projects = new WP_Query(array(
    'post_type'         => 'project',
    'lang'              => substr(get_language_attributes(), 6, 2),
    'post_status'       => 'publish',
    'orderby'           => $atts['orderby'],
    'order'             => $atts['order'],
    'posts_per_page'    => $atts['number_of_items'],
  ));

  // Creating the markup
  $project_html = "<div class='projects-list' data-number-of-items='" . $atts['number_of_items'] . "'>";

  while ($projects->have_posts()) {
    $projects->the_post();

    $categories = get_the_terms($projects->the_ID, 'project_category');
    $categories_html = '';

    if ($categories) {
      foreach ($categories as $category) {
        $categories_html .= '<li>' . $category->name . '</li>';
      }
    }

    $project_html .= "<div id='post-" .  get_the_ID() . "' class='project-item'>";
    $project_html .= "<figure class='photo'>";
    $project_html .= get_the_post_thumbnail($projects->the_ID, 'large', array('alt' => get_the_title()));
    $project_html .= "</figure>";
    $project_html .= "<div class='infos'>";
    $project_html .= "<h4 class='title'>" . get_the_title() . "</h4>";
    $project_html .= "<p class='excerpt'>" . get_the_excerpt() . "</p>";
    $project_html .= "<ul class='categories'>" . $categories_html . "</ul>";
    $project_html .= "</div>";
    $project_html .= "</div>";
  }

  $project_html .= "</div>";

  // Reset the query postdata
  wp_reset_postdata();

  return $project_html;
}
add_shortcode('comfort_projects', 'comfort_projects_shortcode');
