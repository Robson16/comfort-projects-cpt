<?php

// Register Projects Categories Custom Taxonomy
function comfort_project_category()
{
	$labels = array(
		'name'                       => _x('Categories', 'Taxonomy General Name', 'comfortprojects'),
		'singular_name'              => _x('Category', 'Taxonomy Singular Name', 'comfortprojects'),
		'menu_name'                  => __('Categories', 'comfortprojects'),
		'all_items'                  => __('All Categories', 'comfortprojects'),
		'parent_item'                => __('Parent Category', 'comfortprojects'),
		'parent_item_colon'          => __('Parent Category:', 'comfortprojects'),
		'new_item_name'              => __('New Category Name', 'comfortprojects'),
		'add_new_item'               => __('Add New Category', 'comfortprojects'),
		'edit_item'                  => __('Edit Category', 'comfortprojects'),
		'update_item'                => __('Update Category', 'comfortprojects'),
		'view_item'                  => __('View Category', 'comfortprojects'),
		'separate_items_with_commas' => __('Separate categories with commas', 'comfortprojects'),
		'add_or_remove_items'        => __('Add or remove categories', 'comfortprojects'),
		'choose_from_most_used'      => __('Choose from the most used', 'comfortprojects'),
		'popular_items'              => __('Popular Categories', 'comfortprojects'),
		'search_items'               => __('Search Categories', 'comfortprojects'),
		'not_found'                  => __('Not Found', 'comfortprojects'),
		'no_terms'                   => __('No categories', 'comfortprojects'),
		'items_list'                 => __('Categories list', 'comfortprojects'),
		'items_list_navigation'      => __('Categories list navigation', 'comfortprojects'),
	);

	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
	);

	register_taxonomy('project_category', array('project'), $args);
}
add_action('init', 'comfort_project_category', 0);
