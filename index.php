<?php

/**
 * Plugin Name:       Comfort Projects CPT
 * Plugin URI:        https://gitlab.com/Robson16/comfort-projects-cpt
 * Description:       Plugin to add a custom post type named Projects and a shortcode to make the base html output
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Robson H. Rodrigues
 * Author URI:        https://robsonhrodrigues.com.br/
 * Text Domain:       comfortprojects
 * Domain Path:       /languages
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
  exit;
}

define('PME_REVISION', '1.0');
define('PME_PLUGIN_URL', plugin_dir_url(__FILE__));
define('PME_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('PME_PLUGIN_SLUG_PATH', plugin_basename(__FILE__));

/**
 * Custom Post Types
 */
require_once PME_PLUGIN_PATH . '/includes/posttype/projects.php';

/**
 * Custom Taxonomies
 */
require_once PME_PLUGIN_PATH . '/includes/taxonomy/projects-categories.php';

/**
 * Custom Shortcode
 */
require_once PME_PLUGIN_PATH . '/includes/shortcode/projects-list.php';
