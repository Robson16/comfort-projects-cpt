��    6      �      |      |     }     �     �     �     �     �  
   �     �     �          /     @     U     c     p          �     �     �     �  	   �  	   �     �     �            \   .     �     �     �     �     �     �                "     ;     Q     e     w     �     �      �     �     �     
          2     H     V     c  0   q      �  �  �     	     �	     �	     �	     �	     �	  
   
     
  "   &
     I
     f
     {
     �
     �
     �
     �
     �
     �
     	          (     8     H     c     v     �  u   �          $     -     5     =     Q     Z     p      �     �     �     �     �  "   �       
   9  	   D     N     b     t     �     �     �     �  0   �         Add New Category Add New Project Add Project Add or remove categories All Categories All Projects Categories Categories list Categories list navigation Choose from the most used Comfort Projects Comfort Projects CPT Edit Category Edit Project Featured Image Filter projects list Insert into projects New Category Name New Project No categories Not Found Not found Not found in Trash Parent Category Parent Category: Parent Projects: Plugin to add a custom post type named Projects and a shortcode to make the base html output Popular Categories Post Type General NameProjects Post Type Singular NameProject Project Project Archives Projects Projects Attributes Projects list Projects list navigation Remove featured image Robson H. Rodrigues Search Categories Search Project Separate categories with commas Set featured image Taxonomy General NameCategories Taxonomy Singular NameCategory Update Category Update Project Uploaded to this project Use as featured image View Category View Project View Projects https://gitlab.com/Robson16/comfort-projects-cpt https://robsonhrodrigues.com.br/ Project-Id-Version: Comfort Projects CPT
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-02-22 21:25+0000
PO-Revision-Date: 2022-02-22 21:26+0000
Last-Translator: 
Language-Team: Português do Brasil
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.8; wp-5.9.1
X-Domain: comfortprojects Adicionar nova categoria Adicionar novo projeto Adicionar projeto Adicionar ou remover categorias todas as categorias Todos os projetos Categorias Lista de categorias Navegação da lista de categorias Escolha entre os mais usados Projetos de conforto Conforto Projetos CPT Editar categoria Editar projeto Imagem em destaque Filtrar lista de projetos Inserir em projetos Nome da nova categoria Novo projeto Nenhuma categoria Não encontrado Não encontrado Não encontrado na lixeira Categoria Parental Categoria Parental: Projetos Pais: Plugin para adicionar um tipo de postagem personalizado chamado Projects e um shortcode para fazer a saída html base Categorias populares Projetos Projeto Projeto Arquivos do projeto Projetos Atributos de Projetos Lista de projetos Navegação da lista de projetos Remover imagem em destaque Robson H. Rodrigues Categorias de pesquisa Pesquisar projeto Separe as categorias com vírgulas Definir imagem em destaque Categorias Categoria Atualizar categoria Atualizar projeto Carregado para este projeto Use como imagem em destaque Ver categoria Ver projeto Visualizar projetos https://gitlab.com/Robson16/comfort-projects-cpt https://robsonhrodrigues.com.br/ 